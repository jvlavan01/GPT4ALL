---
title: Gpt4all
emoji: 🦀
colorFrom: gray
colorTo: pink
sdk: gradio
sdk_version: 3.24.1
app_file: app.py
pinned: false
duplicated_from: Monster/GPT4ALL
---

Check out the configuration reference at https://huggingface.co/docs/hub/spaces-config-reference
